# Example

Example is a LabVIEW Library for using in template

## Installation

Use the VI Package Manager [VIPM](https://vipm.io) to install the package.

## Usage

```code
VI Code Snippets
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Type of the License](https://choosealicense.com/)

# Author
Author name